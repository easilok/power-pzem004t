#include <PZEM004Tv30.h>
PZEM004Tv30 pzem(11, 12);

void setup() {
  Serial.begin(115200);
}
void loop() {
  
  float voltage = pzem.voltage();
  if (voltage == NAN) voltage = 0;
  //if(voltage != NAN){
    //Serial.print("Voltage: "); Serial.print(voltage); Serial.println("V");
  //} else {
    //Serial.println("Error reading voltage");
  //}
  
  float current = pzem.current();
  if (current == NAN) current = 0;
  //if(current != NAN){
    //Serial.print("Current: "); Serial.print(current); Serial.println("A");
  //} else {
    //Serial.println("Error reading current");
  //}
  
  float power = pzem.power();
  if (power == NAN) power = 0;
  //if(current != NAN){
    //Serial.print("Power: "); Serial.print(power); Serial.println("W");
  //} else {
    //Serial.println("Error reading power");
  //}
  
  float energy = pzem.energy();
  if (energy == NAN) energy = 0;
  //if(current != NAN){
    //Serial.print("Energy: "); Serial.print(energy,3); Serial.println("kWh");
  //} else {
    //Serial.println("Error reading energy");
  //}
  
  //float frequency = pzem.frequency();
  //if(current != NAN){
    //Serial.print("Frequency: "); Serial.print(frequency, 1); Serial.println("Hz");
  //} else {
    //Serial.println("Error reading frequency");
  //}
  
  //float pf = pzem.pf();
  //if(current != NAN){
    //Serial.print("PF: "); Serial.println(pf);
  //} else {
    //Serial.println("Error reading power factor");
    //pf = 0;
  //}

  String str_voltage = String(voltage);
  String str_current = String(current);
  String str_power = String(power);
  String str_energy = String(energy);

  String payload = "{";
  payload += "\"voltage\":"; payload += str_voltage; payload += ",";
  payload += "\"current\":"; payload += str_current; payload += ",";
  payload += "\"power\":"; payload += str_power; payload += ",";
  payload += "\"energy\":"; payload += str_energy; 
  payload += "}";

  Serial.print(payload);
  Serial.println();
  
  delay(5000);
}
